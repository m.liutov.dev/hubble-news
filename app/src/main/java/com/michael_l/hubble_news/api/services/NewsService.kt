package com.michael_l.hubble_news.api.services

import com.haroldadmin.cnradapter.NetworkResponse
import com.michael_l.hubble_news.api.models.ServerError
import com.michael_l.hubble_news.models.DetailedNews
import com.michael_l.hubble_news.models.ListNews
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface NewsService {

    @GET("news")
    suspend fun getNewsPage(@Query("page") pageNumber: Int): NetworkResponse<List<ListNews>, ServerError>

    @GET("news_release/{id}")
    suspend fun getNewsDetails(@Path("id") newsId: String): NetworkResponse<DetailedNews, ServerError>

}