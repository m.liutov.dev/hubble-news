package com.michael_l.hubble_news.api.models

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class ServerError