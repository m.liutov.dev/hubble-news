package com.michael_l.hubble_news.api.paging

import android.accounts.NetworkErrorException
import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.michael_l.hubble_news.api.helpers.interfaces.NewsServiceHelper
import com.michael_l.hubble_news.models.ListNews
import com.michael_l.hubble_news.models.RequestResult
import javax.inject.Inject

class ListNewsPagingSource @Inject constructor(
    private val newsServiceHelper: NewsServiceHelper
) : PagingSource<Int, ListNews>() {
    override val keyReuseSupported: Boolean
        get() = true

    override fun getRefreshKey(state: PagingState<Int, ListNews>): Int? {
        return null
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, ListNews> {
        val nextPage = params.key ?: 1
        val newsRequest = newsServiceHelper.getNewsPage(nextPage)
        if (newsRequest is RequestResult.Error) {
            return LoadResult.Error(NetworkErrorException())
        }
        val news = (newsRequest as RequestResult.Success).result
        val nextKey = if (news.isEmpty()) null else nextPage + 1
        return LoadResult.Page(
            news,
            null, nextKey
        )
    }

    companion object{
        const val PAGE_SIZE = 25
    }

}