package com.michael_l.hubble_news.api.models

sealed class RequestErrorType {

    data class Server(val code: Int) : RequestErrorType()
    object Network : RequestErrorType()
    data class Unknown(val error: Throwable, val code: Int?) : RequestErrorType()

}