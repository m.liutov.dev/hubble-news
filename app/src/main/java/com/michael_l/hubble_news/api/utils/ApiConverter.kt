package com.michael_l.hubble_news.api.utils

import com.haroldadmin.cnradapter.NetworkResponse
import com.michael_l.hubble_news.api.models.RequestErrorType
import com.michael_l.hubble_news.models.RequestResult

class ApiConverter {

    fun <T : Any, U : Any> responseToResult(response: NetworkResponse<T, U>): RequestResult<T, U> =
        when (response) {
            is NetworkResponse.Success -> RequestResult.Success(response.body)
            is NetworkResponse.ServerError -> RequestResult.Error(
                RequestErrorType.Server(response.code),
                response.body
            )
            is NetworkResponse.NetworkError -> RequestResult.Error(RequestErrorType.Network, null)
            is NetworkResponse.UnknownError -> RequestResult.Error(
                RequestErrorType.Unknown(response.error, response.code),
                null
            )
        }
    
}