package com.michael_l.hubble_news.api.helpers.interfaces

import com.michael_l.hubble_news.api.models.ServerError
import com.michael_l.hubble_news.models.DetailedNews
import com.michael_l.hubble_news.models.ListNews
import com.michael_l.hubble_news.models.RequestResult

interface NewsServiceHelper {

    suspend fun getNewsPage(pageNumber: Int): RequestResult<List<ListNews>, ServerError>

    suspend fun getNewsDetails(newsId: String): RequestResult<DetailedNews, ServerError>

}