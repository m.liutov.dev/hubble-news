package com.michael_l.hubble_news.api.helpers.impls

import com.michael_l.hubble_news.api.helpers.interfaces.NewsServiceHelper
import com.michael_l.hubble_news.api.models.ServerError
import com.michael_l.hubble_news.api.services.NewsService
import com.michael_l.hubble_news.api.utils.ApiConverter
import com.michael_l.hubble_news.models.DetailedNews
import com.michael_l.hubble_news.models.ListNews
import com.michael_l.hubble_news.models.RequestResult
import javax.inject.Inject

class NewsServiceHelperImpl @Inject constructor(
    private val newsService: NewsService,
    private val apiConverter: ApiConverter
) : NewsServiceHelper {
    override suspend fun getNewsPage(pageNumber: Int): RequestResult<List<ListNews>, ServerError> {
        return apiConverter.responseToResult(newsService.getNewsPage(pageNumber))
    }

    override suspend fun getNewsDetails(newsId: String): RequestResult<DetailedNews, ServerError> {
        return apiConverter.responseToResult(newsService.getNewsDetails(newsId))
    }
}