package com.michael_l.hubble_news.di

import android.content.Context
import androidx.room.Room
import com.haroldadmin.cnradapter.NetworkResponseAdapterFactory
import com.michael_l.hubble_news.api.helpers.impls.NewsServiceHelperImpl
import com.michael_l.hubble_news.api.helpers.interfaces.NewsServiceHelper
import com.michael_l.hubble_news.api.services.NewsService
import com.michael_l.hubble_news.api.utils.ApiConverter
import com.michael_l.hubble_news.local_data.AppDatabase
import com.michael_l.hubble_news.local_data.ResourceProvider
import com.michael_l.hubble_news.local_data.ResourceProviderImpl
import com.michael_l.hubble_news.local_data.daos.DetailedNewsDao
import com.michael_l.hubble_news.repositories.impls.NewsRepositoryImpl
import com.michael_l.hubble_news.repositories.interfaces.NewsRepository
import com.michael_l.hubble_news.utils.Constants
import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Singleton
    @Provides
    fun provideAppDatabase(@ApplicationContext context: Context): AppDatabase {
        return Room.databaseBuilder(
            context,
            AppDatabase::class.java,
            Constants.APP_DATABASE
        )
            .fallbackToDestructiveMigration()
            .build()
    }

    @Singleton
    @Provides
    fun provideNewsDao(appDatabase: AppDatabase): DetailedNewsDao {
        return appDatabase.detailedNewsDao()
    }

    @Singleton
    @Provides
    fun provideResourceProvider(@ApplicationContext context: Context): ResourceProvider =
        ResourceProviderImpl(context)

    @Provides
    fun provideBaseUrl() = Constants.BASE_URL

    @Provides
    @Singleton
    fun provideOkHttpClient(): OkHttpClient =
        OkHttpClient.Builder()
            .addInterceptor(HttpLoggingInterceptor().apply { setLevel(HttpLoggingInterceptor.Level.BODY) })
            .build()

    @Provides
    @Singleton
    fun provideMoshi(): Moshi = Moshi.Builder()
        .build()

    @Provides
    @Singleton
    fun provideRetrofit(okHttpClient: OkHttpClient, baseUrl: String, moshi: Moshi): Retrofit =
        Retrofit.Builder()
            .addCallAdapterFactory(NetworkResponseAdapterFactory())
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .baseUrl(baseUrl)
            .client(okHttpClient)
            .build()

    @Provides
    @Singleton
    fun provideApiConverter() = ApiConverter()

    @Provides
    @Singleton
    fun provideNewsService(retrofit: Retrofit): NewsService =
        retrofit.create(NewsService::class.java)

    @Provides
    @Singleton
    fun provideNewsServiceHelper(newsServiceHelperImpl: NewsServiceHelperImpl): NewsServiceHelper =
        newsServiceHelperImpl

    @Provides
    @Singleton
    fun provideNewsRepository(newsRepositoryImpl: NewsRepositoryImpl): NewsRepository =
        newsRepositoryImpl

}