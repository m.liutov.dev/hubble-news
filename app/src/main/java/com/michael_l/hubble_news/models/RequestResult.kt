package com.michael_l.hubble_news.models

import com.michael_l.hubble_news.api.models.RequestErrorType


sealed class RequestResult<out T : Any, out U : Any> {

    data class Success<T : Any>(val result: T) : RequestResult<T, Nothing>()

    data class Error<U : Any>(val errorType: RequestErrorType, val errorBody: U?) :
        RequestResult<Nothing, U>()

}