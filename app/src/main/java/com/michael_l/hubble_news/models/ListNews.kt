package com.michael_l.hubble_news.models

import android.os.Parcelable
import com.squareup.moshi.JsonClass

import com.squareup.moshi.Json
import kotlinx.parcelize.Parcelize

@Parcelize
@JsonClass(generateAdapter = true)
data class ListNews(
    @Json(name = "name")
    val name: String,
    @Json(name = "news_id")
    val newsId: String,
    @Json(name = "url")
    val url: String
) : Parcelable