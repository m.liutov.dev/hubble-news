package com.michael_l.hubble_news.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.JsonClass

import com.squareup.moshi.Json

@Entity
@JsonClass(generateAdapter = true)
data class DetailedNews(
    @PrimaryKey
    @Json(name = "news_id")
    val newsId: String,
    @Json(name = "name")
    val name: String,
    @Json(name = "abstract")
    val description: String,
    @Json(name = "credits")
    val credits: String?,
    @Json(name = "keystone_image_2x")
    val keystoneImage2x: String,
    @Json(name = "mission")
    val mission: String?,
    @Json(name = "publication")
    val publication: String?,
    @Json(name = "url")
    val url: String
)