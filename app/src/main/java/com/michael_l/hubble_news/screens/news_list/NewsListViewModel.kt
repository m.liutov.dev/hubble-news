package com.michael_l.hubble_news.screens.news_list

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.cachedIn
import androidx.paging.insertSeparators
import androidx.paging.map
import com.michael_l.hubble_news.models.ListNews
import com.michael_l.hubble_news.repositories.interfaces.NewsRepository
import com.michael_l.hubble_news.utils.Constants
import com.michael_l.hubble_news.utils.SingleLiveEvent
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.map
import javax.inject.Inject

@HiltViewModel
class NewsListViewModel @Inject constructor(
    newsRepository: NewsRepository
) : ViewModel() {

    private val _eventNavigateDetails = SingleLiveEvent<String>()
    val eventNavigateDetails: LiveData<String>
        get() = _eventNavigateDetails

    private val _eventNavigateLink = SingleLiveEvent<String>()
    val eventNavigateLink: LiveData<String>
        get() = _eventNavigateLink

    val news =
        newsRepository.getNewsStream().cachedIn(viewModelScope).map { pagingData ->
            pagingData.map { news ->
                ListNewsUiModel.ItemNews(news)
            }
        }
            .map {
                it.insertSeparators { before: ListNewsUiModel.ItemNews?, after: ListNewsUiModel.ItemNews? ->

                    if (before == null) {
                        //Beginning of the list
                        return@insertSeparators ListNewsUiModel.Contribute
                    }
                    null
                }
            }


    fun onNewsPressed(news: ListNews) {
        _eventNavigateDetails.value = news.newsId
    }

    fun onOpenRepositoryPressed() {
        _eventNavigateLink.value = Constants.REPO_URL
    }

    sealed class ListNewsUiModel {
        data class ItemNews(val news: ListNews) : ListNewsUiModel()
        object Contribute : ListNewsUiModel()
    }

}