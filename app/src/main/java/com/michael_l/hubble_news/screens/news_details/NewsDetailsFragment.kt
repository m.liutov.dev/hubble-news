package com.michael_l.hubble_news.screens.news_details

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.michael_l.hubble_news.databinding.FragmentNewsDetailsBinding
import com.michael_l.hubble_news.utils.ImageUtils
import com.michael_l.hubble_news.utils.Resource
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class NewsDetailsFragment : Fragment() {

    private lateinit var binding: FragmentNewsDetailsBinding
    private val viewModel: NewsDetailsViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentNewsDetailsBinding.inflate(inflater, container, false)
        setObservers()
        binding.retryBtn.setOnClickListener {
            viewModel.onRetryPressed()
        }
        binding.backBtn.setOnClickListener {
            viewModel.onBackPressed()
        }
        binding.openInternetBtn.setOnClickListener {
            viewModel.onOpenInternetPressed()
        }
        return binding.root
    }

    private fun setObservers() {
        viewModel.detailedNews.observe(viewLifecycleOwner) { newsResource ->
            binding.loader.isVisible = newsResource is Resource.Loading
            binding.errorContainer.isVisible = newsResource is Resource.Error
            binding.contentContainer.isVisible = newsResource is Resource.Success
            when (newsResource) {
                is Resource.Error -> {
                    binding.errorTxt.text = newsResource.message
                }
                Resource.Loading -> {
                }
                is Resource.Success -> {
                    val news = newsResource.data
                    Glide.with(this).load(ImageUtils.fixImageLink(news.keystoneImage2x))
                        .into(binding.newsImg)
                    binding.newsTitleTxt.text = news.name
                    binding.newsBodyTxt.text = news.description
                }
            }
        }
        viewModel.eventOpenLink.observe(viewLifecycleOwner) { url ->
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
            startActivity(browserIntent)
        }
        viewModel.eventError.observe(viewLifecycleOwner) { message ->
            Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()
        }
        viewModel.eventNavigateBack.observe(viewLifecycleOwner) {
            findNavController().navigateUp()
        }
    }
}