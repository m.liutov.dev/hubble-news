package com.michael_l.hubble_news.screens.news_list.rcl

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.LoadState
import androidx.paging.LoadStateAdapter
import androidx.recyclerview.widget.RecyclerView
import com.michael_l.hubble_news.R
import com.michael_l.hubble_news.databinding.RclNewsLoadstateBinding

class NewsListLoadStateAdapter(private val retry: () -> Unit) :
    LoadStateAdapter<NewsListLoadStateAdapter.ListNewsLoadStateViewHolder>() {
    override fun onBindViewHolder(holder: ListNewsLoadStateViewHolder, loadState: LoadState) {
        holder.bind(loadState)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        loadState: LoadState
    ): ListNewsLoadStateViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.rcl_news_loadstate, parent, false)
        val binding = RclNewsLoadstateBinding.bind(view)
        return ListNewsLoadStateViewHolder(binding, retry)
    }

    class ListNewsLoadStateViewHolder(
        private val binding: RclNewsLoadstateBinding,
        retry: () -> Unit
    ) : RecyclerView.ViewHolder(binding.root) {

        init {
            binding.retryBtn.setOnClickListener { retry.invoke() }
        }

        fun bind(loadState: LoadState) {
            binding.loader.visibility =
                if (loadState is LoadState.Loading) View.VISIBLE else View.GONE
            binding.retryBtn.visibility =
                if (loadState is LoadState.Error) View.VISIBLE else View.GONE
            binding.errorTxt.visibility =
                if (loadState is LoadState.Error) View.VISIBLE else View.GONE

        }

    }
}