package com.michael_l.hubble_news.screens.news_list.rcl

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.RecyclerView
import com.michael_l.hubble_news.R
import com.michael_l.hubble_news.databinding.RclContributeBinding
import com.michael_l.hubble_news.databinding.RclNewsBinding
import com.michael_l.hubble_news.models.ListNews
import com.michael_l.hubble_news.screens.news_list.NewsListViewModel
import com.michael_l.hubble_news.utils.DataClassDiffCallback
import java.lang.IllegalArgumentException

class NewsListRclAdapter(private val callback: NewsListCallback) :
    PagingDataAdapter<NewsListViewModel.ListNewsUiModel, NewsListRclAdapter.NewsRclViewHolder>(
        DataClassDiffCallback()
    ) {

    interface NewsListCallback {
        fun onNewsPressed(news: ListNews)
        fun onOpenRepositoryPressed()
    }

    override fun onBindViewHolder(holder: NewsRclViewHolder, position: Int) {
        holder.bind()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsRclViewHolder {
        return when (viewType) {
            R.layout.rcl_news -> {
                val view =
                    LayoutInflater.from(parent.context)
                        .inflate(R.layout.rcl_news, parent, false)
                NewsViewHolder(RclNewsBinding.bind(view))
            }
            R.layout.rcl_contribute -> {
                val view =
                    LayoutInflater.from(parent.context)
                        .inflate(R.layout.rcl_contribute, parent, false)
                ContributeViewHolder(RclContributeBinding.bind(view))
            }
            else -> throw IllegalArgumentException("Unknown view type")
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (getItem(position)) {
            is NewsListViewModel.ListNewsUiModel.ItemNews -> R.layout.rcl_news
            is NewsListViewModel.ListNewsUiModel.Contribute -> R.layout.rcl_contribute
            else -> throw UnsupportedOperationException("Unknown view")
        }
    }

    abstract class NewsRclViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        abstract fun bind()
    }

    inner class NewsViewHolder(private val binding: RclNewsBinding) :
        NewsRclViewHolder(binding.root) {
        override fun bind() {
            val item =
                (getItem(bindingAdapterPosition) as NewsListViewModel.ListNewsUiModel.ItemNews).news
            binding.title.text = item.name
            binding.root.setOnClickListener {
                callback.onNewsPressed(item)
            }
        }
    }

    inner class ContributeViewHolder(private val binding: RclContributeBinding) :
        NewsRclViewHolder(binding.root) {

        override fun bind() {
            binding.openBtn.setOnClickListener {
                callback.onOpenRepositoryPressed()
            }
        }
    }
}