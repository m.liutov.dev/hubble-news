package com.michael_l.hubble_news.screens.news_list

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.paging.LoadState
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.michael_l.hubble_news.databinding.FragmentNewsListBinding
import com.michael_l.hubble_news.models.ListNews
import com.michael_l.hubble_news.screens.news_list.rcl.NewsListLoadStateAdapter
import com.michael_l.hubble_news.screens.news_list.rcl.NewsListRclAdapter
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

@AndroidEntryPoint
class NewsListFragment : Fragment() {

    private lateinit var binding: FragmentNewsListBinding
    private val viewModel: NewsListViewModel by viewModels()
    private lateinit var adapter: NewsListRclAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentNewsListBinding.inflate(inflater, container, false)
        setupRecycler()
        setObservers()
        loadNews()
        return binding.root
    }

    private fun loadNews() {
        lifecycleScope.launch {
            viewModel.news.collect { pagingData ->
                adapter.submitData(pagingData)
            }
        }
    }

    private fun setupRecycler() {
        binding.newsRcl.layoutManager = LinearLayoutManager(requireContext())
        binding.newsRcl.addItemDecoration(
            DividerItemDecoration(
                requireContext(),
                LinearLayoutManager.VERTICAL
            )
        )
        adapter = NewsListRclAdapter(object : NewsListRclAdapter.NewsListCallback {
            override fun onNewsPressed(news: ListNews) {
                viewModel.onNewsPressed(news)
            }

            override fun onOpenRepositoryPressed() {
                viewModel.onOpenRepositoryPressed()
            }
        })
        binding.newsRcl.adapter =
            adapter.withLoadStateFooter(NewsListLoadStateAdapter { adapter.retry() })
        adapter.addLoadStateListener { loadState ->
            //Show list if loaded successfully
            binding.newsRcl.visibility =
                if (loadState.source.refresh is LoadState.NotLoading) View.VISIBLE else View.GONE
            //Show loader during loading
            binding.loader.visibility =
                if (loadState.source.refresh is LoadState.Loading) View.VISIBLE else View.GONE
            //Show error container when error occurred
            binding.errorContainer.visibility =
                if (loadState.source.refresh is LoadState.Error) View.VISIBLE else View.GONE
        }
        binding.retryBtn.setOnClickListener {
            adapter.retry()
        }
    }

    private fun setObservers() {
        viewModel.eventNavigateDetails.observe(viewLifecycleOwner) { newsId ->
            val action =
                NewsListFragmentDirections.actionNewsListFragmentToNewsDetailsFragment(newsId)
            findNavController().navigate(action)
        }
        viewModel.eventNavigateLink.observe(viewLifecycleOwner) { link ->
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(link))
            startActivity(browserIntent)
        }
    }
}