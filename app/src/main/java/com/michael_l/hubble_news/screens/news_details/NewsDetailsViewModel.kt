package com.michael_l.hubble_news.screens.news_details

import androidx.lifecycle.*
import com.michael_l.hubble_news.R
import com.michael_l.hubble_news.api.models.RequestErrorType
import com.michael_l.hubble_news.di.IoDispatcher
import com.michael_l.hubble_news.local_data.ResourceProvider
import com.michael_l.hubble_news.models.DetailedNews
import com.michael_l.hubble_news.models.RequestResult
import com.michael_l.hubble_news.repositories.interfaces.NewsRepository
import com.michael_l.hubble_news.utils.Resource
import com.michael_l.hubble_news.utils.SingleLiveEvent
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class NewsDetailsViewModel @Inject constructor(
    private val newsRepository: NewsRepository,
    savedStateHandle: SavedStateHandle,
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher,
    private val resourceProvider: ResourceProvider
) : ViewModel() {

    private val _detailedNews = MutableLiveData<Resource<DetailedNews>>(Resource.Loading)
    val detailedNews: LiveData<Resource<DetailedNews>>
        get() = _detailedNews

    private val _eventOpenLink = SingleLiveEvent<String>()
    val eventOpenLink: LiveData<String>
        get() = _eventOpenLink

    private val _eventError = SingleLiveEvent<String>()
    val eventError: LiveData<String>
        get() = _eventError

    private val _eventNavigateBack = SingleLiveEvent<Boolean>()
    val eventNavigateBack: LiveData<Boolean>
        get() = _eventNavigateBack

    private val newsId = savedStateHandle.get<String>(NEWS_ID_ARG)!!

    init {
        loadNewsDetails()
    }

    private fun loadNewsDetails() {
        viewModelScope.launch(ioDispatcher) {
            when (val result = newsRepository.getNewsDetails(newsId)) {
                is RequestResult.Error -> {
                    val errorMessage = when (result.errorType) {
                        RequestErrorType.Network -> resourceProvider.getString(R.string.genericError_internet)
                        is RequestErrorType.Server -> resourceProvider.getString(R.string.genericError_server)
                        is RequestErrorType.Unknown -> resourceProvider.getString(R.string.genericError_unknown)
                    }
                    _detailedNews.postValue(Resource.Error(errorMessage))
                }
                is RequestResult.Success -> {
                    _detailedNews.postValue(Resource.Success(result.result))
                }
            }
        }
    }

    fun onRetryPressed() {
        loadNewsDetails()
    }

    fun onOpenInternetPressed() {
        val news = _detailedNews.value!! as? Resource.Success
        if (news == null) {
            _eventError.value = resourceProvider.getString(R.string.newsDetails_error_link)
            return
        }
        _eventOpenLink.value = news.data.url
    }

    fun onBackPressed() {
        _eventNavigateBack.value = true
    }

    companion object{

        const val NEWS_ID_ARG = "newsId"

    }
}