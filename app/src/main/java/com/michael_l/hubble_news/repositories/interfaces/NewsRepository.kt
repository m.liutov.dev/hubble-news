package com.michael_l.hubble_news.repositories.interfaces

import androidx.paging.PagingData
import com.michael_l.hubble_news.api.models.ServerError
import com.michael_l.hubble_news.models.DetailedNews
import com.michael_l.hubble_news.models.ListNews
import com.michael_l.hubble_news.models.RequestResult
import kotlinx.coroutines.flow.Flow

interface NewsRepository {

    fun getNewsStream(): Flow<PagingData<ListNews>>

    suspend fun getNewsDetails(newsId: String): RequestResult<DetailedNews, ServerError>

}