package com.michael_l.hubble_news.repositories.impls

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.michael_l.hubble_news.api.helpers.interfaces.NewsServiceHelper
import com.michael_l.hubble_news.api.models.ServerError
import com.michael_l.hubble_news.api.paging.ListNewsPagingSource
import com.michael_l.hubble_news.local_data.daos.DetailedNewsDao
import com.michael_l.hubble_news.models.DetailedNews
import com.michael_l.hubble_news.models.ListNews
import com.michael_l.hubble_news.models.RequestResult
import com.michael_l.hubble_news.repositories.interfaces.NewsRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class NewsRepositoryImpl @Inject constructor(
    private val newsServiceHelper: NewsServiceHelper,
    private val newsDao: DetailedNewsDao
) : NewsRepository {

    override fun getNewsStream(): Flow<PagingData<ListNews>> {
        return Pager(config = PagingConfig(ListNewsPagingSource.PAGE_SIZE), pagingSourceFactory = {
            ListNewsPagingSource(newsServiceHelper)
        }).flow
    }

    override suspend fun getNewsDetails(newsId: String): RequestResult<DetailedNews, ServerError> {
        when (val apiResult = newsServiceHelper.getNewsDetails(newsId)) {
            is RequestResult.Error -> {
                //Check if we have this News in cache
                val cachedNews = newsDao.getById(newsId)
                if (cachedNews != null) {
                    //If we do, return as successful result
                    return RequestResult.Success(cachedNews)
                }
                //Return the error
                return apiResult
            }
            is RequestResult.Success -> {
                //Saving updated News to cache
                newsDao.insert(apiResult.result)
                return apiResult
            }
        }
    }
}