package com.michael_l.hubble_news.utils

object ImageUtils {

    /**
     * Links for images coming from server don't have
     * "https:/", so it is added here
     */
    fun fixImageLink(link: String): String {
        val prefix = if (!link.startsWith("https:/")) "https:/" else ""
        return prefix + link
    }

}