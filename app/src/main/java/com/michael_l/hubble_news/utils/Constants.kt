package com.michael_l.hubble_news.utils

object Constants {

    const val APP_DATABASE = "app_database"

    const val BASE_URL = "https://hubblesite.org/api/v3/"

    const val REPO_URL = "https://gitlab.com/m.liutov.dev/hubble-news"

}