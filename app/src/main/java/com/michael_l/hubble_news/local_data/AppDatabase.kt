package com.michael_l.hubble_news.local_data

import androidx.room.Database
import androidx.room.RoomDatabase
import com.michael_l.hubble_news.local_data.daos.DetailedNewsDao
import com.michael_l.hubble_news.models.DetailedNews

@Database(entities = [DetailedNews::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {

    abstract fun detailedNewsDao(): DetailedNewsDao

}