package com.michael_l.hubble_news.local_data.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.michael_l.hubble_news.models.DetailedNews

@Dao
interface DetailedNewsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(detailedNews: DetailedNews)

    @Query("SELECT * FROM detailednews WHERE newsId=:newsId")
    fun getById(newsId: String):DetailedNews?

}