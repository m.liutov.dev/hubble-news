package com.michael_l.hubble_news.local_data

interface ResourceProvider {

    fun getString(id: Int): String

}