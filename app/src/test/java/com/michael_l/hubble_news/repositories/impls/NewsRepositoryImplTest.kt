package com.michael_l.hubble_news.repositories.impls

import android.os.Build
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.michael_l.hubble_news.api.helpers.interfaces.NewsServiceHelper
import com.michael_l.hubble_news.api.models.RequestErrorType
import com.michael_l.hubble_news.api.models.ServerError
import com.michael_l.hubble_news.local_data.daos.DetailedNewsDao
import com.michael_l.hubble_news.models.DetailedNews
import com.michael_l.hubble_news.models.RequestResult
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.setMain
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.times
import org.mockito.MockitoAnnotations
import org.robolectric.annotation.Config

@Config(sdk = [Build.VERSION_CODES.O_MR1])
@RunWith(AndroidJUnit4::class)
@ExperimentalCoroutinesApi
class NewsRepositoryImplTest {

    private val testDispatcher = TestCoroutineDispatcher()

    @Mock
    private lateinit var newsServiceHelper: NewsServiceHelper

    @Mock
    private lateinit var newsDao: DetailedNewsDao

    private val detailedNews = DetailedNews(
        "News id",
        "Name",
        "Description",
        "Credits",
        "Image",
        "Mission",
        "Publication",
        "Url"
    )

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setupBefore() {
        MockitoAnnotations.openMocks(this)
        Dispatchers.setMain(testDispatcher)
    }

    @Test
    fun getNewsDetailsWorkingInternet() {
        runBlocking {
            val newsRepositoryImpl = NewsRepositoryImpl(newsServiceHelper, newsDao)
            Mockito.`when`(newsServiceHelper.getNewsDetails(detailedNews.newsId))
                .thenReturn(RequestResult.Success(detailedNews))

            val result = newsRepositoryImpl.getNewsDetails(detailedNews.newsId)

            assert(result is RequestResult.Success)
            assert((result as RequestResult.Success).result == detailedNews)
            //Check that new item was inserted into cache
            Mockito.verify(newsDao, times(1)).insert(detailedNews)
        }
    }

    @Test
    fun getNewsDetailsNoInternetInCache() {
        runBlocking {
            val newsRepositoryImpl = NewsRepositoryImpl(newsServiceHelper, newsDao)
            Mockito.`when`(newsServiceHelper.getNewsDetails(detailedNews.newsId))
                .thenReturn(RequestResult.Error(RequestErrorType.Network, ServerError()))
            Mockito.`when`(newsDao.getById(detailedNews.newsId))
                .thenReturn(detailedNews)

            val result = newsRepositoryImpl.getNewsDetails(detailedNews.newsId)

            assert(result is RequestResult.Success)
            assert((result as RequestResult.Success).result == detailedNews)
        }
    }

    @Test
    fun getNewsDetailsNoInternetNoCache() {
        runBlocking {
            val newsRepositoryImpl = NewsRepositoryImpl(newsServiceHelper, newsDao)
            Mockito.`when`(newsServiceHelper.getNewsDetails(detailedNews.newsId))
                .thenReturn(RequestResult.Error(RequestErrorType.Network, ServerError()))
            Mockito.`when`(newsDao.getById(detailedNews.newsId))
                .thenReturn(null)

            val result = newsRepositoryImpl.getNewsDetails(detailedNews.newsId)

            assert(result is RequestResult.Error)
            assert((result as RequestResult.Error).errorType == RequestErrorType.Network)
        }
    }

}