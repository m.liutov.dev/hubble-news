package com.michael_l.hubble_news.screens.news_details

import android.os.Build
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.SavedStateHandle
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.michael_l.hubble_news.api.models.RequestErrorType
import com.michael_l.hubble_news.local_data.ResourceProvider
import com.michael_l.hubble_news.models.DetailedNews
import com.michael_l.hubble_news.models.RequestResult
import com.michael_l.hubble_news.repositories.interfaces.NewsRepository
import com.michael_l.hubble_news.utils.Resource
import com.michael_l.hubble_news.utils.getOrAwaitValue
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.setMain
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.robolectric.annotation.Config

@Config(sdk = [Build.VERSION_CODES.O_MR1])
@RunWith(AndroidJUnit4::class)
@ExperimentalCoroutinesApi
class NewsDetailsViewModelTest {

    private val testDispatcher = TestCoroutineDispatcher()

    @Mock
    private lateinit var newsRepository: NewsRepository

    @Mock
    private lateinit var resourceProvider: ResourceProvider

    private lateinit var savedStateHandle: SavedStateHandle
    private val newsId = "test_id"

    private val detailedNews = DetailedNews(
        newsId,
        "Name",
        "Description",
        "Credits",
        "Image",
        "Mission",
        "Publication",
        "Url"
    )

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setupBefore() {
        MockitoAnnotations.openMocks(this)
        Dispatchers.setMain(testDispatcher)
        savedStateHandle = SavedStateHandle()
        savedStateHandle.set(NewsDetailsViewModel.NEWS_ID_ARG, newsId)
    }

    @Test
    fun loadNewsDetailsSuccess() {
        runBlocking {
            Mockito.`when`(newsRepository.getNewsDetails(newsId))
                .thenReturn(RequestResult.Success(detailedNews))
            val viewModel =
                NewsDetailsViewModel(
                    newsRepository,
                    savedStateHandle,
                    testDispatcher,
                    resourceProvider
                )

            val loadedNews = viewModel.detailedNews.getOrAwaitValue()
            assert(loadedNews is Resource.Success)
            assert((loadedNews as Resource.Success).data == detailedNews)
        }
    }

    @Test
    fun loadNewsDetailsFail() {
        runBlocking {
            val errorMsg = "ERROR"
            Mockito.`when`(newsRepository.getNewsDetails(newsId))
                .thenReturn(RequestResult.Error(RequestErrorType.Network, null))
            Mockito.`when`(resourceProvider.getString(Mockito.anyInt())).thenReturn(errorMsg)
            val viewModel =
                NewsDetailsViewModel(
                    newsRepository,
                    savedStateHandle,
                    testDispatcher,
                    resourceProvider
                )

            val loadedNews = viewModel.detailedNews.getOrAwaitValue()
            assert(loadedNews is Resource.Error)
            assert((loadedNews as Resource.Error).message == errorMsg)
        }
    }

    @Test
    fun onRetryPressed() {
        runBlocking {
            Mockito.`when`(newsRepository.getNewsDetails(newsId))
                .thenReturn(RequestResult.Error(RequestErrorType.Network, null))
            Mockito.`when`(resourceProvider.getString(Mockito.anyInt())).thenReturn("Soma error")
            val viewModel =
                NewsDetailsViewModel(
                    newsRepository,
                    savedStateHandle,
                    testDispatcher,
                    resourceProvider
                )
            Mockito.`when`(newsRepository.getNewsDetails(newsId))
                .thenReturn(RequestResult.Success(detailedNews))

            viewModel.onRetryPressed()

            val loadedNews = viewModel.detailedNews.getOrAwaitValue()
            assert(loadedNews is Resource.Success)
            assert((loadedNews as Resource.Success).data == detailedNews)
        }
    }

    @Test
    fun onOpenInternetPressed() {
        runBlocking {
            Mockito.`when`(newsRepository.getNewsDetails(newsId))
                .thenReturn(RequestResult.Success(detailedNews))
            val viewModel =
                NewsDetailsViewModel(
                    newsRepository,
                    savedStateHandle,
                    testDispatcher,
                    resourceProvider
                )

            viewModel.onOpenInternetPressed()

            val navigateInternet = viewModel.eventOpenLink.getOrAwaitValue()
            assert(navigateInternet == detailedNews.url)
        }
    }

    @Test
    fun onBackPressed() {
        runBlocking {
            Mockito.`when`(newsRepository.getNewsDetails(newsId))
                .thenReturn(RequestResult.Success(detailedNews))
            val viewModel =
                NewsDetailsViewModel(
                    newsRepository,
                    savedStateHandle,
                    testDispatcher,
                    resourceProvider
                )

            viewModel.onBackPressed()

            val navigateBackEvent = viewModel.eventNavigateBack.getOrAwaitValue()
            assert(navigateBackEvent)
        }
    }
}