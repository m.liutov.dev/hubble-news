### Hubble News

This is a sample project, made to demonstrate usage of Android libraries and technologies.

The project consists of 2 screens:
- List of News
- News details

List of news is loaded from Hubble News' public API and is paginated using Paging 3 library. To demonstrate the feature to insert separators between items, the list begins with a call to contribute to the project.

Details of news are cached and can be opened without internet connection, once in cache. List of news is not cached.

Used technologies and libraries:
- MVVM
- LiveData
- Room
- View binding
- Recyclerview
- Paging 3
- Glide
- Navigation components, safeargs
- Hilt
- Retrofit
- Moshi
- Kotlin Coroutines
- Kotlin Flow (only for paging)
- ConstraintLayout
- Unit tests, Mockito
